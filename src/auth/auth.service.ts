import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { User, UserDocument, UserSchema } from '../users/schemas/user.schema';
import { Model } from 'mongoose';
import { AuthDto } from './dto/auth.dto';
import * as bcrypt from 'bcrypt';

@Injectable()
export class AuthService {
  constructor(@InjectModel(User.name) private userModel: Model<UserDocument>) {}
  async login(authDto: AuthDto) {
    const user = await this.userModel.findOne({ email: authDto.email });
    if (!user) {
      return 'Unable to find User ! please check your email';
    }
    if (!(await bcrypt.compare(authDto.password, user.password))) {
      return 'Password incorrect';
    } else {
      return user;
    }
  }
}
