import { Body, Controller, HttpStatus, Post, Response } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { AuthDto } from './dto/auth.dto';
import { AuthService } from './auth.service';

@Controller('auth')
export class AuthController {
  constructor(
    private authService: AuthService,
    private jwtService: JwtService,
  ) {}

  @Post()
  async login(@Body() authDto: AuthDto, @Response() res) {
    if (!authDto.email) {
      return await res.status(HttpStatus.BAD_REQUEST).json({
        message: 'Enter your email !',
      });
    } else if (!authDto.password) {
      return await res.status(HttpStatus.BAD_REQUEST).json({
        message: 'Enter your Password !',
      });
    } else {
      const user = await this.authService.login(authDto);
      const payload = { user: user };
      const jwt = await this.jwtService.signAsync(payload);
      return res.status(HttpStatus.OK).json({
        token: jwt,
        user,
      });
    }
  }
}
