import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types, Schema as MongooseSchema } from 'mongoose';
import { Product } from 'src/products/products.schema';

export type UserDocument = User & Document;

@Schema()
export class User {
  @Prop()
  username: String;

  @Prop()
  email: String;

  @Prop()
  password: String;

  @Prop({ type: MongooseSchema.Types.ObjectId, ref: 'Product' })
  cart: [Product];
}

export const UserSchema = SchemaFactory.createForClass(User);
