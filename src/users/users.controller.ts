import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Response,
  HttpStatus,
  UseGuards,
} from '@nestjs/common';

import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { JwtAuthGuard } from 'src/utils/guard/auth-guard.guard';
import { EventPattern } from '@nestjs/microservices';

@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @EventPattern('Hello')
  async hello(data: string) {
    console.log(`This is MongoServer ! ${data}`);
  }

  @UseGuards(JwtAuthGuard)
  @Post()
  async create(@Body() createUserDto: CreateUserDto, @Response() res) {
    if (
      !createUserDto.email ||
      !createUserDto.password ||
      !createUserDto.username
    ) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        message: 'Please fill up all Fields !',
      });
    }

    const user = await this.usersService.create(createUserDto);
    if (!user) {
      return res.status(HttpStatus.UNPROCESSABLE_ENTITY).json({
        message: 'User registration Failed !',
      });
    }
    return res.status(HttpStatus.OK).json({
      message: 'User registered successfully !',
      user,
    });
  }

  @UseGuards(JwtAuthGuard)
  @Get()
  async findAll(@Response() res) {
    const users = await this.usersService.findAll();
    if (users.length == 0) {
      return res.status(HttpStatus.NOT_FOUND).json({
        message: 'Nothing !',
      });
    }
    return res.status(HttpStatus.OK).json({
      users,
    });
  }

  @UseGuards(JwtAuthGuard)
  @Get(':id')
  async findOne(@Param('id') id: string, @Response() res) {
    const user = await this.usersService.findOne(id);
    if (!user) {
      return res.status(HttpStatus.NOT_FOUND).json({
        message: 'User not found',
      });
    }
    return res.status(HttpStatus.OK).json({
      user,
    });
  }

  @UseGuards(JwtAuthGuard)
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateUserDto: UpdateUserDto) {
    return this.usersService.update(id, updateUserDto);
  }

  @UseGuards(JwtAuthGuard)
  @Delete(':id')
  async remove(@Param('id') id: string, @Response() res) {
    const user = await this.usersService.remove(id);
    if (!user) {
      return res.status(HttpStatus.NOT_FOUND).json({
        message: 'User not found',
      });
    }
    return res.status(HttpStatus.OK).json({
      message: 'User removed from List',
    });
  }
}
